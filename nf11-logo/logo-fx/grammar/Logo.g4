grammar Logo; 

@header {
  package logoparsing;
}

INT : '0' | [1-9][0-9]*;
STR : [a-zA-Z]+;
WS : [ \t\r\n]+ -> skip ;

programme : (liste_procedures)? liste_instructions ;
liste_procedures : (declaration)+;
liste_instructions : (instruction)+;

string:
    STR;

bool:
    expr ('='|'!='|'<'|'>'|'<='|'>=') expr;


expr:
    expr '^' expr       #exp
  | expr ('*'|'/') expr #mult
  | expr ('+'|'-') expr #sum
  | INT                 #int
  | '(' expr ')'        #bracket
  | 'hasard' expr       #hasard
  | 'loop'              #loop
  | ':'string           #var
;

declaration :
    'pour' string '(' (':'STR)+ ')' '{' (liste_instructions) '}';


instruction :
    'av' expr                               # av
  | 'td' expr                               # td
  | 'tg' expr                               # tg
  | 'bc'                                    # bc
  | 'lc'                                    # lc
  | 've'                                    # ve
  | 're' expr                               # re
  | 'fpos' expr expr                        # fpos
  | 'fcc' expr                              # fcc
  | 'repete' expr '['liste_instructions']'  # repete
  | 'donne' '"'string expr                  # donne
  | 'si' bool '['liste_instructions']'('['liste_instructions']')? #si
  | 'tq' bool '['liste_instructions']'      #tq
  | 'appel' string '('(expr)+')'            #appel
;
/*
pour f (:a :c) {
	fcc :c
	av :a
}

appel f 4 40

*/