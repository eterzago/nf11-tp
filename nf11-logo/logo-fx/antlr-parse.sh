#!/bin/bash

export SAVECLASSPATH=$CLASSPATH
export CLASSPATH="./lib/antlr-4.7.1-complete.jar;./bin"

java org.antlr.v4.gui.TestRig logoparsing.Logo programme -gui
programs/logo-prg.txt
export CLASSPATH=$SAVECLASSPATH
