package tools;

import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;

public class Procedure {
    private String nom;
    private ArrayList<String> params;
    private ParseTree instructions;

    public Procedure(String nom, ArrayList<String> params, ParseTree instructions) {
        this.nom = nom;
        this.params = params;
        this.instructions = instructions;
    }

    public ArrayList<String> getParams() {
        return params;
    }

    public ParseTree getInstructions() {
        return instructions;
    }
}
