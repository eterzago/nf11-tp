package tools;

import java.util.Stack;

public class Pile {
    private Stack<TableSymboles> pile;

    public Pile() {
        this.pile = new Stack<>();
    }

    /**
     * Empile la table de symboles au sommet de la pile
     * @param t table de symboles à empiler
     */
    public void empiler(TableSymboles t) {
        this.getPile().push(t);
    }

    /**
     * Retourne le sommet de la pile et le retire de la pile
     * @return sommet de la pile
     */
    public TableSymboles depiler() {
        return this.getPile().pop();
    }

    /**
     * Retourne le sommet de la pile, sans le dépiler
     * @return sommet de la pile
     */
    public TableSymboles sommet() {
        return this.getPile().peek();
    }


    public Stack<TableSymboles> getPile() {
        return pile;
    }
    public void setPile(Stack<TableSymboles> pile) {
        this.pile = pile;
    }
}
