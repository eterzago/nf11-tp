package tools;

import java.util.HashMap;

public class TableSymboles {
    private HashMap<String,Double> map;

    public TableSymboles() {
        this.map = new HashMap<>();
    }

    public void add(String key, Double val)
    {
        this.map.put(key,val);
    }

    public Double get(String key)
    {
        return this.map.get(key);
    }

    /*public HashMap<String, Double> getMap() {
        return map;
    }

    public void setMap(HashMap<String, Double> map) {
        this.map = map;
    }*/
}
