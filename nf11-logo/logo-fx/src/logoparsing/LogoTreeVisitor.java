package logoparsing;

import javafx.beans.property.IntegerProperty;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import javafx.scene.canvas.Canvas;
import logogui.Log;
import logogui.Traceur;
import logoparsing.LogoParser.AvContext;
import logoparsing.LogoParser.TdContext;
import logoparsing.LogoParser.TgContext;
import org.antlr.v4.runtime.tree.TerminalNode;
import tools.Pile;
import tools.Procedure;
import tools.TableProcedures;
import tools.TableSymboles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class LogoTreeVisitor extends logoparsing.LogoBaseVisitor<Integer> {
	Pile pile;
	TableProcedures procedures;
	Traceur traceur;
	ParseTreeProperty<Double> atts = new ParseTreeProperty<>();

	public LogoTreeVisitor() {
		super();
	}
	public void initialize(Canvas canvas) {
		  TableSymboles symboles = new TableSymboles();
		  pile = new Pile();
		  pile.empiler(symboles);
		  procedures = new TableProcedures();
	      traceur = new Traceur();
	      traceur.setGraphics(canvas);
    }
	public void setAttValue(ParseTree node, double value) { 
		atts.put(node, value);
	}
	public double getAttValue(ParseTree node) { return atts.get(node); }
	@Override
	public Integer visitAv(AvContext ctx) {
		visit(ctx.expr());
		traceur.avance(getAttValue(ctx.expr()));
		Log.appendnl("visitAv");
		return 0;
	}

	@Override
	public Integer visitTg(TgContext ctx) {
		int n = visit(ctx.expr());

		if(n == 0)
		{
			traceur.tg(getAttValue(ctx.expr()));
			Log.append("visitTg" );
		}
		else
		{
			Log.append("Problem:visitTg");
			System.err.println("problem Tg\n");
		}
		return 0;
	}


	@Override
	public Integer visitTd(logoparsing.LogoParser.TdContext ctx) {

		int n = visit(ctx.expr());

		if(n == 0)
		{
			traceur.td(getAttValue(ctx.expr()));
			Log.append("visitTd");
		}
		else
		{
			Log.append("Problem:visitTd");
			System.err.println("problem Td\n");
		}
		return 0;
	}

	@Override
	public Integer visitInt(logoparsing.LogoParser.IntContext ctx) {
		setAttValue(ctx, Double.valueOf(ctx.INT().getText()));
		return 0;
	}

	@Override
	public Integer visitBc(logoparsing.LogoParser.BcContext ctx) {
		traceur.bc();
		Log.appendnl("visitBc\n");
		return 0;
	}

	@Override
	public Integer visitLc(logoparsing.LogoParser.LcContext ctx) {
		traceur.lc();
		Log.appendnl("visitLc\n");
		return 0;
	}

	@Override
	public Integer visitVe(logoparsing.LogoParser.VeContext ctx) {
		traceur.ve();
		Log.appendnl("visitVe\n");
		return 0;
	}

	@Override
	public Integer visitRe(logoparsing.LogoParser.ReContext ctx) {
		visit(ctx.expr());
		traceur.recule(getAttValue(ctx.expr()));
		Log.appendnl("visitRe\n");
		return 0;
	}

	@Override
	public Integer visitFpos(logoparsing.LogoParser.FposContext ctx) {
		String intText1 = ctx.expr(0).getText();
		String intText2 = ctx.expr(1).getText();
		traceur.fPos(Integer.valueOf(intText1), Integer.valueOf(intText2));
		Log.appendnl("visitFpos\n");
		return 0;
	}

	@Override
	public Integer visitFcc(logoparsing.LogoParser.FccContext ctx) {
		visit(ctx.expr());
		traceur.changeCouleur(Integer.valueOf((int)getAttValue(ctx.expr())));
		Log.appendnl("visitFcc\n");
		return 0;
	}

	@Override
	public Integer visitMult(logoparsing.LogoParser.MultContext ctx) {
		visit(ctx.expr(0));
		visit(ctx.expr(1));
		Double left = getAttValue(ctx.expr(0));
		Double right = getAttValue(ctx.expr(1));
		Double result =
				ctx.getChild(1).getText().equals("*") ?
						left * right : left / right;
		setAttValue(ctx, result);
		Log.appendnl("visitMult\n");
		return 0;
	}

	@Override
	public Integer visitBracket(logoparsing.LogoParser.BracketContext ctx) {
		visit(ctx.expr());
		setAttValue(ctx,getAttValue(ctx.expr()));
		Log.appendnl("visitBracket\n");

		return 0;
	}

	@Override
	public Integer visitSum(logoparsing.LogoParser.SumContext ctx) {
		visit(ctx.expr(0));
		visit(ctx.expr(1));
		Double left = getAttValue(ctx.expr(0));
		Double right = getAttValue(ctx.expr(1));
		Double result =
				ctx.getChild(1).getText().equals("+") ?
						left + right : left - right;
		setAttValue(ctx,result);
		Log.appendnl("visitSum\n");

		return 0;
	}

	@Override
	public Integer visitExp(logoparsing.LogoParser.ExpContext ctx) {
		visit(ctx.expr(0));
		visit(ctx.expr(1));
		Double left = getAttValue(ctx.expr(0));
		Double right = getAttValue(ctx.expr(1));
		Double result = Math.pow(left,right);
		setAttValue(ctx,result);
		Log.appendnl("visitExp\n");

		return 0;
	}

	@Override
	public Integer visitHasard(logoparsing.LogoParser.HasardContext ctx) {
		visit(ctx.expr());
		Random r = new Random();
		setAttValue(ctx,r.nextInt());
		return 0;
	}

	@Override
	public Integer visitLoop(logoparsing.LogoParser.LoopContext ctx) {
		return 0;
		// TODO loop = compteur de boucle
	}

	@Override
	public Integer visitRepete(logoparsing.LogoParser.RepeteContext ctx) {
		visit(ctx.expr());
		Double boucle = getAttValue(ctx.expr());
		for(int i = 0; i < boucle; i += 1)
		{
			visit(ctx.liste_instructions());
		}
		return 0;
	}

	@Override
	public Integer visitDonne(logoparsing.LogoParser.DonneContext ctx) {
		visit(ctx.expr());
		String key = ctx.string().STR().getText();
		Double var = getAttValue(ctx.expr());
		this.pile.sommet().add(key,var);
		return 0;
	}

	@Override
	public Integer visitVar(logoparsing.LogoParser.VarContext ctx) {
		String key = ctx.string().STR().getText();
		Double var = this.pile.sommet().get(key);
		setAttValue(ctx, var);
		return 0;
	}

	@Override
	public Integer visitBool(logoparsing.LogoParser.BoolContext ctx) {
		visit(ctx.expr(0));
		visit(ctx.expr(1));

		Double left = getAttValue(ctx.expr(0));
		Double right = getAttValue(ctx.expr(1));

		switch(ctx.getChild(1).getText())
		{
			case "=":
				setAttValue(ctx,(left==right)? 1:0);
				break;
			case "!=":
				setAttValue(ctx,(left!=right)? 1:0);
				break;
			case ">":
				setAttValue(ctx,(left>right)? 1:0);
				break;
			case "<":
				setAttValue(ctx,(left<right)? 1:0);
				break;
			case ">=":
				setAttValue(ctx,(left>=right)? 1:0);
				break;
			case "<=":
				setAttValue(ctx,(left<=right)? 1:0);
				break;
		}

		return 0;
	}

	@Override
	public Integer visitSi(logoparsing.LogoParser.SiContext ctx) {
		visit(ctx.bool());
		double bool = getAttValue(ctx.bool());
		if(bool == 1)
		{
			visit(ctx.liste_instructions(0));
		}
		else
		{
			if(ctx.liste_instructions().size() == 2)
			{
				visit(ctx.liste_instructions(1));
			}
		}

		return 0;
	}

	@Override
	public Integer visitTq(logoparsing.LogoParser.TqContext ctx) {
		visit(ctx.bool());
		double bool = getAttValue(ctx.bool());

		while(bool == 1)
		{
			visit(ctx.liste_instructions());
			visit(ctx.bool());
			bool = getAttValue(ctx.bool());
		}

		return 0;
	}

	@Override
	public Integer visitDeclaration(logoparsing.LogoParser.DeclarationContext ctx) {
		String nom = ctx.string().getText();

		List<TerminalNode> params = ctx.STR();
		ArrayList<String> listeParams = new ArrayList<>();
		for (TerminalNode param : params) {
			listeParams.add(param.getText());
		}

		ParseTree instructions = ctx.liste_instructions();

		Procedure p = new Procedure(nom,listeParams,instructions);
		this.procedures.put(nom,p);
		return 0;
	}

	@Override
	public Integer visitAppel(logoparsing.LogoParser.AppelContext ctx) {
		this.pile.empiler(new TableSymboles());

		Procedure p = procedures.get(ctx.string().getText());

		int i = 0;
		for(logoparsing.LogoParser.ExprContext expr : ctx.expr()) {
			visit(expr);
			this.pile.sommet().add(p.getParams().get(i), getAttValue(expr));
			i += 1;
		}

		visit(p.getInstructions());

		this.pile.depiler();
		return 0;
	}
}
