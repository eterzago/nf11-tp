/*
 * Created on 12 sept. 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package logogui;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.awt.*;

public class Traceur {
	private double initx = 300, inity = 300;   // position initiale
	private double posx = initx, posy = inity; // position courante
	private int angle = 90;
	private double teta;

	private Color couleur = Color.BLACK;
	private boolean crayonBaisse = true;

	GraphicsContext gc;
	public Traceur() {
		setTeta();
	}

	public void setGraphics(Canvas canvas) {
		gc = canvas.getGraphicsContext2D();	
	}
	
	private int toInt(double a) {
		return (int) Math.round(a);
	}
	private void addLine(int x1, int y1, int x2, int y2) {
		if(crayonBaisse)
		{
            gc.setStroke(this.couleur);
            gc.strokeLine(x1, y1, x2, y2);
		}
	}
	public void avance(double r) {
		double a = posx + r * Math.cos(teta) ;
		double b = posy - r * Math.sin(teta) ;
		int x1 = toInt(posx);
		int y1 = toInt(posy);
		int x2 = toInt(a);
		int y2 = toInt(b);
		addLine(x1, y1, x2, y2);
		posx = a;
		posy = b;
	}

	public void recule(double r) {
		double a = posx - r * Math.cos(teta) ;
		double b = posy + r * Math.sin(teta) ;
		int x1 = toInt(posx);
		int y1 = toInt(posy);
		int x2 = toInt(a);
		int y2 = toInt(b);
		addLine(x1, y1, x2, y2);
		posx = a;
		posy = b;
	}
	
	public void td(double r) {
		angle = (angle - toInt(r)) % 360;
		setTeta();
	}

	public void tg(double r) {
		angle = (angle + toInt(r)) % 360;
		setTeta();
	}

	public void bc()
	{
		this.crayonBaisse = true;
	}
	public void lc()
	{
		this.crayonBaisse = false;
	}

	public void ve()
	{
		gc.clearRect(0,0,gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
	}

	public void fPos(double x, double y)
	{
		posx = x;
		posy = y;
	}

	public void changeCouleur(int c)
    {
        if(c > Couleurs.BLANC)
        {
            c = c%8;
        }

        switch(c)
        {
            case Couleurs.NOIR:
                this.couleur = Color.BLACK;
                break;
            case Couleurs.BLANC:
                this.couleur = Color.WHITE;
				break;
            case Couleurs.BLEU:
                this.couleur = Color.BLUE;
				break;
            case Couleurs.BLEU_CLAIR:
                this.couleur = Color.LIGHTBLUE;
				break;
            case Couleurs.VERT:
                this.couleur = Color.GREEN;
				break;
            case Couleurs.ROUGE:
                this.couleur = Color.RED;
				break;
            case Couleurs.JAUNE:
                this.couleur = Color.YELLOW;
				break;
            case Couleurs.VIOLET:
                this.couleur = Color.PURPLE;
				break;
            default:
                System.err.println("Couleur non reconnue.");
                this.couleur = Color.BLACK;
				break;
        }
        gc.setStroke(this.couleur);
    }


	
	private void setTeta() {
		teta = Math.toRadians(angle);
	}
}
