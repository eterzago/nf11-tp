package logogui;

public class Couleurs {

    public static final int NOIR = 0;
    public static final int ROUGE = 1;
    public static final int VERT = 2;
    public static final int JAUNE = 3;
    public static final int BLEU = 4;
    public static final int VIOLET = 5;
    public static final int BLEU_CLAIR = 6;
    public static final int BLANC = 7;
}
